package com.br.inter.invest.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CompanyEntityTest {

    @Test
    void shouldGenerateHash() {
        CompanyEntity company = new CompanyEntity();

        company.generatedHash();

        assertThat(company.getHash()).isNotNull();
    }
}
