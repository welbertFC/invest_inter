package com.br.inter.invest.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PurchaseOrderEntityTest {

    @Test
    void shouldGenerateHash() {
        PurchaseOrderEntity entity = new PurchaseOrderEntity();

        entity.generatedHash();

        assertThat(entity.getHash()).isNotNull();

    }
}
