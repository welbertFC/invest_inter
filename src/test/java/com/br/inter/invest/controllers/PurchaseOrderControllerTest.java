package com.br.inter.invest.controllers;

import com.br.inter.invest.controller.purchaseOrder.PurchaseOrderController;
import com.br.inter.invest.controller.purchaseOrder.dto.CompanyPurchaseDto;
import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseRequest;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.usecase.purchase.createPurchase.CreatePurchaseUseCase;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.AcquiredCompaniesDto;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseOutput;
import com.br.inter.invest.usecase.purchase.createPurchase.mapper.CreatePurchaseMapper;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.ListPurchaseByCpfOrCnpjUseCase;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseInput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseOutput;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static com.br.inter.invest.util.ConstantsUtils.PURCHASE_ORDER_URI_V1;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PurchaseOrderController.class)
class PurchaseOrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreatePurchaseUseCase createPurchase;

    @MockBean
    private ListPurchaseByCpfOrCnpjUseCase listPurchaseByCpfOrCnpj;

    private final ObjectMapper mapper = new ObjectMapper();


    @Test
    void shouldReturnCreatedWhenPostAValidRequest() throws Exception {
        PurchaseRequest request = new PurchaseRequest("test", "95.197.223/0001-05", 2, BigDecimal.TEN);

        PurchaseInput input = CreatePurchaseMapper.convertRequestToInput(request);

        CompanyEntity companyEntity = new CompanyEntity(
                1L, UUID.randomUUID(), "test", "TEST1", BigDecimal.TEN, true, null, LocalDateTime.now(), LocalDateTime.now());

        PurchaseOrderEntity entity = CreatePurchaseMapper.convertInputToEntity(input,
                new AcquiredCompaniesDto(Collections.singletonList(companyEntity), BigDecimal.TEN));

        PurchaseOutput output = CreatePurchaseMapper.convertEntityToOutput(entity);

        when(createPurchase.execute(input)).thenReturn(output);

        String json = mapper.writeValueAsString(request);

        mockMvc.perform(post(PURCHASE_ORDER_URI_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(content().string(this.mapper.writeValueAsString(CreatePurchaseMapper.convertOutputToResponse(output))));

    }

    @Test
    void shouldReturnBadRequestAndErrorListWhenPostAnInvalidRequest() throws Exception {
        PurchaseRequest request = new PurchaseRequest("", "", null, null);
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("name", Arrays.asList("must not be blank", "size must be between 3 and 100"));
        errors.put("quantities", Collections.singletonList("must not be null"));
        errors.put("value", Collections.singletonList("must not be null"));
        errors.put("cpfcnpj", Collections.singletonList("must not be blank"));

        String json = mapper.writeValueAsString(request);

        mockMvc.perform(post(PURCHASE_ORDER_URI_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is(errors)))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    void shouldReturnBadRequestAndErrorListWhenPostAnInvalidCPFOrCNPJRequest() throws Exception {
        PurchaseRequest request = new PurchaseRequest("teste", "12", 2, BigDecimal.TEN);
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("cpfcnpj", Collections.singletonList("Invalid cpf or cpnj"));

        String json = mapper.writeValueAsString(request);

        mockMvc.perform(post(PURCHASE_ORDER_URI_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is(errors)))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    void shouldReturnOkWhenGetAll() throws Exception {
        ListPurchaseInput input = new ListPurchaseInput("123", PageRequest.of(0, 20));

        CompanyPurchaseDto purchaseDto = new CompanyPurchaseDto("test", "test1", 1, BigDecimal.ZERO);

        ListPurchaseOutput output = new ListPurchaseOutput("test",
                "1234",
                Sets.set(purchaseDto),
                BigDecimal.TEN,
                BigDecimal.ZERO
        );


        when(listPurchaseByCpfOrCnpj.execute(input)).thenReturn(new PageImpl<>(singletonList(output)));

        mockMvc.perform(get(String.format("%s/{cpfcnpj}", PURCHASE_ORDER_URI_V1), input.getCpfcnpj())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
