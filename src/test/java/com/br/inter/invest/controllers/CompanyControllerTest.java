package com.br.inter.invest.controllers;

import com.br.inter.invest.controller.company.CompanyController;
import com.br.inter.invest.controller.company.dto.CompanyRequest;
import com.br.inter.invest.controller.company.dto.CompanyUpdateActiveRequest;
import com.br.inter.invest.controller.company.dto.CompanyUpdatePriceRequest;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.usecase.company.createCompany.CreateCompanyUseCase;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyOutput;
import com.br.inter.invest.usecase.company.createCompany.mapper.CreateCompanyMapper;
import com.br.inter.invest.usecase.company.deleteCompany.DeleteCompanyUseCase;
import com.br.inter.invest.usecase.company.deleteCompany.dto.DeleteCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.ListCompanyUseCase;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyOutput;
import com.br.inter.invest.usecase.company.updateActiveCompany.UpdateActiveCompanyUseCase;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyInput;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyOutput;
import com.br.inter.invest.usecase.company.updateActiveCompany.mapper.UpdateActiveMapper;
import com.br.inter.invest.usecase.company.updatePriceCompany.UpdatePriceCompanyUseCase;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyInput;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyOutput;
import com.br.inter.invest.usecase.company.updatePriceCompany.mapper.UpdatePriceMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static com.br.inter.invest.util.ConstantsUtils.COMPANY_URI_CONTROLLER_V1;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreateCompanyUseCase createCompany;

    @MockBean
    private ListCompanyUseCase listCompany;

    @MockBean
    private DeleteCompanyUseCase deleteCompany;

    @MockBean
    private UpdatePriceCompanyUseCase updatePriceCompany;

    @MockBean
    private UpdateActiveCompanyUseCase updateActiveCompany;

    private final ObjectMapper mapper = new ObjectMapper();


    @Test
    void shouldReturnCreatedWhenPostAValidRequest() throws Exception {
        CompanyRequest request = new CompanyRequest("test", "test1", BigDecimal.TEN, true);

        CreateCompanyInput input = CreateCompanyMapper.convertRequestToInput(request);

        CompanyEntity entity = CreateCompanyMapper.convertInputToEntity(input);

        CreateCompanyOutput output = CreateCompanyMapper.convertEntityToOutput(entity);

        when(createCompany.execute(input)).thenReturn(output);

        String json = mapper.writeValueAsString(request);

        mockMvc.perform(post(COMPANY_URI_CONTROLLER_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(content().string(this.mapper.writeValueAsString(CreateCompanyMapper.convertOutputToResponse(output))));
    }

    @Test
    void shouldReturnBadRequestAndErrorListWhenPostAnInvalidRequest() throws Exception {
        CompanyRequest request = new CompanyRequest("", "", null, true);

        Map<String, List<String>> errors = new HashMap<>();
        errors.put("name", Arrays.asList("must not be blank", "size must be between 3 and 100"));
        errors.put("ticker", Arrays.asList("must not be blank", "size must be between 2 and 20"));
        errors.put("price", Collections.singletonList("must not be null"));

        String json = mapper.writeValueAsString(request);

        mockMvc.perform(post(COMPANY_URI_CONTROLLER_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is(errors)))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    void shouldReturnAcceptWhenPatchPriceAValidRequest() throws Exception {

        UUID hash = UUID.randomUUID();
        CompanyUpdatePriceRequest request = new CompanyUpdatePriceRequest(BigDecimal.TEN);

        UpdatePriceCompanyInput input = UpdatePriceMapper.convertRequestToInput(
                hash, request.getPrice());

        CompanyEntity entity = new CompanyEntity(
                1L, hash, "test", "TEST1", BigDecimal.TEN, true, null, LocalDateTime.now(), LocalDateTime.now());

        UpdatePriceCompanyOutput output = UpdatePriceMapper.convertEntityToOutput(entity);

        String json = mapper.writeValueAsString(request);

        when(updatePriceCompany.execute(input)).thenReturn(output);

        mockMvc.perform(patch(String.format("%s/price/{hash}", COMPANY_URI_CONTROLLER_V1), hash)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());

        verify(updatePriceCompany).execute(input);
    }

    @Test
    void shouldReturnAcceptWhenPatchActiveAValidRequest() throws Exception {

        UUID hash = UUID.randomUUID();
        CompanyUpdateActiveRequest request = new CompanyUpdateActiveRequest(true);

        UpdateActiveCompanyInput input = UpdateActiveMapper.convertRequestToInput(hash, request.getActive());

        CompanyEntity entity = new CompanyEntity(
                1L, hash, "test", "TEST1", BigDecimal.TEN, true, null, LocalDateTime.now(), LocalDateTime.now());

        UpdateActiveCompanyOutput output = UpdateActiveMapper.convertEntityToOutput(entity);

        String json = mapper.writeValueAsString(request);

        when(updateActiveCompany.execute(input)).thenReturn(output);

        mockMvc.perform(patch(String.format("%s/active/{hash}", COMPANY_URI_CONTROLLER_V1), hash)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());

        verify(updateActiveCompany).execute(input);
    }

    @Test
    void shouldReturnOkWhenGetAll() throws Exception {
        ListCompanyInput input = new ListCompanyInput(true, PageRequest.of(0, 20));
        ListCompanyOutput output = new ListCompanyOutput(UUID.randomUUID(),
                "test",
                "TEST1",
                BigDecimal.TEN,
                true,
                LocalDateTime.now(),
                LocalDateTime.now());

        when(listCompany.execute(input)).thenReturn(new PageImpl<>(singletonList(output)));

        mockMvc.perform(
                        MockMvcRequestBuilders.get(COMPANY_URI_CONTROLLER_V1)
                                .accept(MediaType.APPLICATION_JSON)
                                .param("active", String.valueOf(true)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnOKWhenDeleteValidRequest() throws Exception {
        DeleteCompanyInput input = new DeleteCompanyInput(UUID.randomUUID());

        mockMvc.perform(
                        MockMvcRequestBuilders.delete(String.format("%s/{hash}", COMPANY_URI_CONTROLLER_V1), input.getHash())
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }
}
