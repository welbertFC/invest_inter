package com.br.inter.invest.useCase.company;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.updatePriceCompany.UpdatePriceCompanyUseCase;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyInput;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UpdatePriceCompanyUseCaseTest {

    @InjectMocks
    private UpdatePriceCompanyUseCase updatePriceCompanyUseCase;

    @Mock
    private CompanyService service;

    @Test
    void shouldUpdatePriceCompany() {
        UpdatePriceCompanyInput input = UpdatePriceCompanyInput.builder()
                .price(BigDecimal.TEN)
                .hash(UUID.randomUUID())
                .build();

        CompanyEntity entity = CompanyEntity.builder().price(BigDecimal.ONE).build();

        when(service.findByHash(input.getHash())).thenReturn(entity);
        when(service.persistedCompany(entity)).thenReturn(entity);

        UpdatePriceCompanyOutput result = updatePriceCompanyUseCase.execute(input);

        assertThat(result.getPrice()).isEqualTo(input.getPrice().setScale(2, RoundingMode.HALF_UP));
    }
}
