package com.br.inter.invest.useCase.company;

import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.createCompany.CreateCompanyUseCase;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import com.br.inter.invest.usecase.company.createCompany.validation.ValidateCreateCompany;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CreateCompanyUseCaseTest {

    @InjectMocks
    private CreateCompanyUseCase createCompany;

    @Mock
    private CompanyService service;

    @Mock
    private List<ValidateCreateCompany> validate;

    @Test
    void shouldCallValidationWhenSaveACompany() {
        CreateCompanyInput companyInput = CreateCompanyInput.builder()
                .name("test")
                .active(true)
                .ticker("test1")
                .price(BigDecimal.TEN)
                .build();

        validate.forEach(validation ->
                verify(validation, times(1)).validate(companyInput));
    }
}
