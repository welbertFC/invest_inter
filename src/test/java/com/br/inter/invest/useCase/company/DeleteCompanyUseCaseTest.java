package com.br.inter.invest.useCase.company;


import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.deleteCompany.DeleteCompanyUseCase;
import com.br.inter.invest.usecase.company.deleteCompany.dto.DeleteCompanyInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DeleteCompanyUseCaseTest {

    @InjectMocks
    private DeleteCompanyUseCase deleteCompanyUseCase;

    @Mock
    private CompanyService service;

    @Test
    void shouldDeleteCompany() {
        DeleteCompanyInput input = DeleteCompanyInput.builder().hash(UUID.randomUUID()).build();

        deleteCompanyUseCase.execute(input);

        verify(service, times(1)).delete(input.getHash());
    }
}
