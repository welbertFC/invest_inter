package com.br.inter.invest.useCase.purchase;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.service.PurchaseOrderService;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.ListPurchaseByCpfOrCnpjUseCase;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseInput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ListPurchaseByCpfOrCnpjUseCaseTest {

    @InjectMocks
    private ListPurchaseByCpfOrCnpjUseCase listPurchaseByCpfOrCnpjUseCase;

    @Mock
    private PurchaseOrderService service;

    @Test
    void shouldReturnListPurchase() {
        PageRequest pageable = PageRequest.of(20, 20);

        ListPurchaseInput input = ListPurchaseInput
                .builder()
                .cpfcnpj("1234")
                .pageable(pageable)
                .build();

        CompanyEntity companyEntity = CompanyEntity.builder()
                .name("test")
                .price(BigDecimal.valueOf(2))
                .active(true)
                .hash(UUID.randomUUID())
                .creation(LocalDateTime.now())
                .ticker("TEST")
                .build();

        PurchaseOrderEntity entity = PurchaseOrderEntity
                .builder()
                .investedAmount(BigDecimal.TEN)
                .companies(singletonList(companyEntity))
                .moneyChange(BigDecimal.ONE)
                .creation(LocalDateTime.now())
                .hash(UUID.randomUUID())
                .build();


        when(service.listPurchaseOrderCpfOrCnpj("1234", pageable)).thenReturn(new PageImpl<>(singletonList(entity)));

        Page<ListPurchaseOutput> result = listPurchaseByCpfOrCnpjUseCase.execute(input);

        assertThat(result).isNotNull();
    }
}
