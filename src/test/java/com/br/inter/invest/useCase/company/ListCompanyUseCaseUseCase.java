package com.br.inter.invest.useCase.company;


import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.listCompany.ListCompanyUseCase;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class ListCompanyUseCaseUseCase {

    @InjectMocks
    private ListCompanyUseCase listCompanyUseCase;

    @Mock
    private CompanyService service;

    @Test
    void shouldReturnListCompany() {
        PageRequest pageable = PageRequest.of(20, 20);

        CompanyEntity entity = CompanyEntity.builder().build();

        when(service.listCompanyPageable(true, pageable)).thenReturn(new PageImpl<>(singletonList(entity)));

        ListCompanyInput input = ListCompanyInput.builder()
                .active(true)
                .pageable(pageable)
                .build();

        Page<ListCompanyOutput> result = listCompanyUseCase.execute(input);

        assertThat(result).isNotNull();

        verify(service, times(1)).listCompanyPageable(input.getActive(), pageable);
    }
}
