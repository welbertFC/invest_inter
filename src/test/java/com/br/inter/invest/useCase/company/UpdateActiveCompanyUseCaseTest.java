package com.br.inter.invest.useCase.company;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.updateActiveCompany.UpdateActiveCompanyUseCase;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyInput;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class UpdateActiveCompanyUseCaseTest {

    @InjectMocks
    private UpdateActiveCompanyUseCase updateActiveCompanyUseCase;

    @Mock
    private CompanyService service;

    @Test
    void shouldUpdateActiveCompany() {
        UpdateActiveCompanyInput input = UpdateActiveCompanyInput.builder()
                .hash(UUID.randomUUID())
                .active(false)
                .build();

        CompanyEntity entity = CompanyEntity.builder().active(true).build();

        when(service.findByHash(input.getHash())).thenReturn(entity);
        when(service.persistedCompany(entity)).thenReturn(entity);

        UpdateActiveCompanyOutput result = updateActiveCompanyUseCase.execute(input);

        assertThat(result.getActive()).isFalse();

    }
}
