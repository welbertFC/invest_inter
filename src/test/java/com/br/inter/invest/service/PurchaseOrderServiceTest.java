package com.br.inter.invest.service;

import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.repository.PurchaseOrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PurchaseOrderServiceTest {

    @InjectMocks
    private PurchaseOrderService service;

    @Mock
    private PurchaseOrderRepository repository;

    @Test
    void shouldPersistedWhenEntityIsValid() {
        PurchaseOrderEntity newEntity = PurchaseOrderEntity.builder().build();

        when(repository.save(any())).thenReturn(PurchaseOrderEntity.builder()
                .id(1L)
                .build());

        PurchaseOrderEntity result = service.persistedPurchaseOrder(newEntity);

        assertThat(result.getId()).isNotNull();
    }

    @Test
    void shouldListPurchaseOrderByCpfOrCnpj() {
        PageRequest pageable = PageRequest.of(20, 20);
        String cpf = "123456789";

        when(repository.findBycpfcnpj(cpf, pageable)).thenReturn(Page.empty());

        service.listPurchaseOrderCpfOrCnpj(cpf, pageable);

        verify(repository, times(1)).findBycpfcnpj(cpf, pageable);
    }


}
