package com.br.inter.invest.service;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.exception.ObjectNotFoundException;
import com.br.inter.invest.repository.CompanyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CompanyServiceTest {

    @InjectMocks
    private CompanyService service;

    @Mock
    private CompanyRepository repository;


    @Test
    void shouldPersistedWhenEntityIsValid() {
        CompanyEntity newEntity = CompanyEntity.builder().build();

        when(repository.save(any())).thenReturn(CompanyEntity.builder()
                .id(1L)
                .build());

        CompanyEntity result = service.persistedCompany(newEntity);

        assertThat(result.getId()).isNotNull();
    }

    @Test
    void shouldListCompanyPageableActiveTrue() {
        PageRequest pageable = PageRequest.of(20, 20);

        when(repository.findAllByActive(true, pageable)).thenReturn(Page.empty());

        service.listCompanyPageable(true, pageable);

        verify(repository, times(1)).findAllByActive(true, pageable);
    }

    @Test
    void shouldListCompanyPageableActiveNull() {
        PageRequest pageable = PageRequest.of(20, 20);

        when(repository.findAll(pageable)).thenReturn(Page.empty());

        service.listCompanyPageable(null, pageable);

        verify(repository, times(1)).findAll(pageable);
    }

    @Test
    void shouldListCompany() {

        when(repository.findAll()).thenReturn(Collections.emptyList());

        service.listCompany();

        verify(repository, times(1)).findAll();
    }

    @Test
    void shouldFindByHash() {
        CompanyEntity entity = CompanyEntity.builder().id(1L).hash(UUID.randomUUID()).build();

        when(repository.findByHash(entity.getHash())).thenReturn(Optional.of(entity));

        CompanyEntity result = service.findByHash(entity.getHash());

        assertThat(entity).isEqualTo(result);
        verify(repository, times(1)).findByHash(entity.getHash());
    }

    @Test
    void shouldThrowExceptionWhenFindByHash() {
        UUID hash = UUID.randomUUID();

        when(repository.findByHash(hash)).thenReturn(Optional.empty());

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class,
                () -> service.findByHash(hash));

        assertThat(exception).hasMessage("Object not found");
    }

    @Test
    void shouldDeleteAnCompany() {
        CompanyEntity entity = CompanyEntity.builder().id(1L).hash(UUID.randomUUID()).build();

        when(repository.findByHash(entity.getHash())).thenReturn(Optional.of(entity));

        service.delete(entity.getHash());

        verify(repository, times(1)).delete(entity);
    }

    @Test
    void shouldThrowExceptionDeleteAnCompanyWhenHashInvalid() {
        CompanyEntity entity = CompanyEntity.builder().id(1L).hash(UUID.randomUUID()).build();

        when(repository.findByHash(entity.getHash())).thenReturn(Optional.empty());

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class,
                () -> service.delete(entity.getHash()));

        assertThat(exception).hasMessage("Object not found");
    }


}
