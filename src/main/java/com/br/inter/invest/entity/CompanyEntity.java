package com.br.inter.invest.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class CompanyEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID hash;

    private String name;

    private String ticker;

    private BigDecimal price;

    private Boolean active;

    @ManyToMany(
            mappedBy = "companies",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    private List<PurchaseOrderEntity> purchases = new ArrayList<>();

    @CreatedDate
    private LocalDateTime creation;

    @LastModifiedDate
    private LocalDateTime updateAt;

    public void generatedHash() {
        this.hash = UUID.randomUUID();
    }
}


