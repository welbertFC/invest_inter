package com.br.inter.invest.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
public class PurchaseOrderEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID hash;

    private String name;

    private String cpfcnpj;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Purchase_Company",
            joinColumns = @JoinColumn(name = "id_purchase"),
            inverseJoinColumns = @JoinColumn(name = "id_company")
    )
    private List<CompanyEntity> companies = new ArrayList<>();

    private BigDecimal investedAmount;

    private BigDecimal moneyChange;

    @CreatedDate
    private LocalDateTime creation;

    public void generatedHash() {
        this.hash = UUID.randomUUID();
    }
}
