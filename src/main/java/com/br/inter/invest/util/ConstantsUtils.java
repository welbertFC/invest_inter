package com.br.inter.invest.util;

public interface ConstantsUtils {

    String COMPANY_URI_CONTROLLER_V1 = "/v1/companies";
    String PURCHASE_ORDER_URI_V1 = "/v1/purchasesOrder";
}
