package com.br.inter.invest.util;

public interface ValidationUseCase<T> {

    void validate(T useCase);
}
