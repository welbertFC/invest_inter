package com.br.inter.invest.exception;

public class EmptyListException extends RuntimeException {

    public EmptyListException(String message) {
        super(message);
    }
}
