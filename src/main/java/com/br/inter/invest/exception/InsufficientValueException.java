package com.br.inter.invest.exception;

public class InsufficientValueException extends RuntimeException {

    public InsufficientValueException(String message) {
        super(message);
    }
}
