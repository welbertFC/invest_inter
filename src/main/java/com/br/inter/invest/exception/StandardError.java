package com.br.inter.invest.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StandardError<T> {

    private Integer status;
    private String message;
    private T error;
    private long timeStamp;
    private String path;
}
