package com.br.inter.invest.exception;

public class IllegalBusinessStateObjectException extends RuntimeException{

    public IllegalBusinessStateObjectException(String message){super(message);}

    public static IllegalBusinessStateObjectException tickerIsEquals(){
        return new IllegalBusinessStateObjectException("this ticker already exists, please enter another one");
    }
}
