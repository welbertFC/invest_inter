package com.br.inter.invest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class ResourceExceptionHandler {

    public static final String VALIDATION_ERROR = "Validation error";
    public static final String OBJECT_NOT_FOUND = "object not found";
    public static final String ILLEGAL_BUSINESS_STATE = "illegal business state";
    public static final String DESERIALIZE_VALUE_OF_TYPE = "JSON parse error: Cannot deserialize value of type";
    public static final String EMPTY_LIST = "Empty List";
    public static final String INSUFFICIENT_VALUE = "Insufficient Value";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardError<Map<String, Set<String>>>> validation(
            MethodArgumentNotValidException exception,
            HttpServletRequest request
    ) {

        Map<String, Set<String>> errors = exception.getFieldErrors().stream()
                .collect(Collectors.groupingBy(FieldError::getField))
                .entrySet().parallelStream().map(it -> new AbstractMap.SimpleEntry<>(
                        it.getKey(),
                        it.getValue().parallelStream()
                                .map(FieldError::getDefaultMessage)
                                .collect(Collectors.toSet())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return ResponseEntity.badRequest().body(new StandardError<>(
                HttpStatus.BAD_REQUEST.value(),
                VALIDATION_ERROR,
                errors,
                Calendar.getInstance().getTimeInMillis(),
                request.getRequestURI()));
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<StandardError<String>> objectNotFound(
            ObjectNotFoundException exception, HttpServletRequest request) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new StandardError<>(
                        HttpStatus.NOT_FOUND.value(),
                        OBJECT_NOT_FOUND,
                        exception.getMessage(),
                        Calendar.getInstance().getTimeInMillis(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(IllegalBusinessStateObjectException.class)
    public ResponseEntity<StandardError<String>> illegalBusinessState(
            IllegalBusinessStateObjectException exception,
            HttpServletRequest request
    ) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(
                new StandardError<>(
                        HttpStatus.CONFLICT.value(),
                        ILLEGAL_BUSINESS_STATE,
                        exception.getMessage(),
                        Calendar.getInstance().getTimeInMillis(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<StandardError<String>> notReadable(
            HttpMessageNotReadableException exception,
            HttpServletRequest request
    ) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new StandardError<>(
                        HttpStatus.BAD_REQUEST.value(),
                        DESERIALIZE_VALUE_OF_TYPE,
                        exception.getLocalizedMessage(),
                        Calendar.getInstance().getTimeInMillis(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(EmptyListException.class)
    public ResponseEntity<StandardError<String>> emptyList(
            EmptyListException exception,
            HttpServletRequest request
    ) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new StandardError<>(
                        HttpStatus.BAD_REQUEST.value(),
                        EMPTY_LIST,
                        exception.getMessage(),
                        Calendar.getInstance().getTimeInMillis(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(InsufficientValueException.class)
    public ResponseEntity<StandardError<String>> insufficientValue(
            InsufficientValueException exception,
            HttpServletRequest request
    ) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new StandardError<>(
                        HttpStatus.BAD_REQUEST.value(),
                        INSUFFICIENT_VALUE,
                        exception.getMessage(),
                        Calendar.getInstance().getTimeInMillis(),
                        request.getRequestURI()));
    }
}
