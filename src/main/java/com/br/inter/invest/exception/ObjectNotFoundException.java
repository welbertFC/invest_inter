package com.br.inter.invest.exception;

public class ObjectNotFoundException extends RuntimeException {

    public static final String OBJECT_NOT_FOUND = "Object not found";

    public ObjectNotFoundException() {
        super(OBJECT_NOT_FOUND);
    }
}
