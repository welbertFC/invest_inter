package com.br.inter.invest.controller.purchaseOrder.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyPurchaseDto {

    private String name;
    private String ticker;
    private Integer quantities;
    private BigDecimal totalValue;
}
