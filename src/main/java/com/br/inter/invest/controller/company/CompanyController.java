package com.br.inter.invest.controller.company;

import com.br.inter.invest.controller.company.dto.CompanyRequest;
import com.br.inter.invest.controller.company.dto.CompanyResponse;
import com.br.inter.invest.controller.company.dto.CompanyUpdateActiveRequest;
import com.br.inter.invest.controller.company.dto.CompanyUpdatePriceRequest;
import com.br.inter.invest.usecase.company.createCompany.CreateCompanyUseCase;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyOutput;
import com.br.inter.invest.usecase.company.createCompany.mapper.CreateCompanyMapper;
import com.br.inter.invest.usecase.company.deleteCompany.DeleteCompanyUseCase;
import com.br.inter.invest.usecase.company.deleteCompany.dto.DeleteCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.ListCompanyUseCase;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyOutput;
import com.br.inter.invest.usecase.company.listCompany.mapper.ListCompanyMapper;
import com.br.inter.invest.usecase.company.updateActiveCompany.UpdateActiveCompanyUseCase;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyOutput;
import com.br.inter.invest.usecase.company.updateActiveCompany.mapper.UpdateActiveMapper;
import com.br.inter.invest.usecase.company.updatePriceCompany.UpdatePriceCompanyUseCase;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyOutput;
import com.br.inter.invest.usecase.company.updatePriceCompany.mapper.UpdatePriceMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

import static com.br.inter.invest.util.ConstantsUtils.COMPANY_URI_CONTROLLER_V1;


@RestController
@RequestMapping(COMPANY_URI_CONTROLLER_V1)
@RequiredArgsConstructor
public class CompanyController {

    private final CreateCompanyUseCase createCompany;
    private final ListCompanyUseCase listCompany;
    private final DeleteCompanyUseCase deleteCompany;
    private final UpdatePriceCompanyUseCase updatePriceCompany;
    private final UpdateActiveCompanyUseCase updateActiveCompany;

    @PostMapping
    public ResponseEntity<CompanyResponse> insert(@RequestBody @Valid CompanyRequest request) {
        CreateCompanyOutput companyOutput = createCompany.execute(
                CreateCompanyMapper.convertRequestToInput(request));

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{hash}")
                .buildAndExpand(companyOutput.getHash())
                .toUri();

        return ResponseEntity.created(uri).body(CreateCompanyMapper.convertOutputToResponse(companyOutput));
    }

    @GetMapping
    public ResponseEntity<Page<CompanyResponse>> list(
            Pageable pageable, @RequestParam(value = "active", required = false) final Boolean active
    ) {
        Page<ListCompanyOutput> companies = listCompany.execute(new ListCompanyInput(active, pageable));
        return ResponseEntity.ok(companies.map(ListCompanyMapper::convertOutputToResponse));
    }

    @DeleteMapping("/{hash}")
    public ResponseEntity<Void> delete(@PathVariable UUID hash) {
        deleteCompany.execute(DeleteCompanyInput.builder().hash(hash).build());
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/price/{hash}")
    public ResponseEntity<CompanyResponse> updatePrice(@PathVariable UUID hash,
                                                       @RequestBody @Valid CompanyUpdatePriceRequest request) {
        UpdatePriceCompanyOutput companyOutput = updatePriceCompany.execute(
                UpdatePriceMapper.convertRequestToInput(hash, request.getPrice()));

        return ResponseEntity.ok(UpdatePriceMapper.convertOutputToResponse(companyOutput));
    }

    @PatchMapping("/active/{hash}")
    @Caching(evict = {
            @CacheEvict(value = "listCompanyPageable", allEntries = true),
            @CacheEvict(value = "listCompany", allEntries = true),
            @CacheEvict(value = "listCompanyOrderByPrice", allEntries = true),
    })
    public ResponseEntity<CompanyResponse> updatePrice(@PathVariable UUID hash,
                                                       @RequestBody @Valid CompanyUpdateActiveRequest request) {
        UpdateActiveCompanyOutput companyOutput = updateActiveCompany.execute(
                UpdateActiveMapper.convertRequestToInput(hash, request.getActive()));

        return ResponseEntity.ok(UpdateActiveMapper.convertOutputToResponse(companyOutput));
    }
}
