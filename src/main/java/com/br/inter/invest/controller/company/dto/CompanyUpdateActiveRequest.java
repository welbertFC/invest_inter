package com.br.inter.invest.controller.company.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyUpdateActiveRequest implements Serializable {

    @NotNull
    private Boolean active;
}
