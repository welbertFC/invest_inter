package com.br.inter.invest.controller.purchaseOrder;

import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseRequest;
import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseResponse;
import com.br.inter.invest.usecase.purchase.createPurchase.CreatePurchaseUseCase;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseOutput;
import com.br.inter.invest.usecase.purchase.createPurchase.mapper.CreatePurchaseMapper;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.ListPurchaseByCpfOrCnpjUseCase;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseInput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseOutput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.mapper.ListPurchaseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

import static com.br.inter.invest.util.ConstantsUtils.PURCHASE_ORDER_URI_V1;

@RestController
@RequestMapping(PURCHASE_ORDER_URI_V1)
@RequiredArgsConstructor
public class PurchaseOrderController {

    private final CreatePurchaseUseCase purchase;
    private final ListPurchaseByCpfOrCnpjUseCase listPurchaseByCpfOrCnpj;

    @PostMapping
    public ResponseEntity<PurchaseResponse> insert(@RequestBody @Valid PurchaseRequest request) {
        PurchaseOutput purchaseOrder = purchase.execute(CreatePurchaseMapper.convertRequestToInput(request));
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{name}")
                .buildAndExpand(purchaseOrder.getName())
                .toUri();

        return ResponseEntity.created(uri).body(CreatePurchaseMapper.convertOutputToResponse(purchaseOrder));
    }

    @GetMapping("/{cpfcnpj}")
    public ResponseEntity<Page<PurchaseResponse>> list(Pageable pageable, @PathVariable String cpfcnpj) {
        Page<ListPurchaseOutput> listPurchaseOutputs = listPurchaseByCpfOrCnpj
                .execute(new ListPurchaseInput(cpfcnpj, pageable));

        return ResponseEntity.ok(listPurchaseOutputs.map(ListPurchaseMapper::convertOutputToResponse));

    }
}
