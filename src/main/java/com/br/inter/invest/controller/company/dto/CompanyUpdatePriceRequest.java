package com.br.inter.invest.controller.company.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyUpdatePriceRequest implements Serializable {

    @NotNull
    private BigDecimal price;
}
