package com.br.inter.invest.controller.purchaseOrder.dto;

import com.br.inter.invest.util.annotation.IsCPFOrCNPJ;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseRequest {

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    @NotBlank
    @IsCPFOrCNPJ
    private String cpfcnpj;

    @NotNull
    private Integer quantities;

    @NotNull
    private BigDecimal value;

}
