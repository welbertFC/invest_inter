package com.br.inter.invest.controller.purchaseOrder.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponse {

    private String name;
    private String cpfcnpj;
    private Set<CompanyPurchaseDto> companies;
    private BigDecimal totalValue;
    private BigDecimal changeMoney;
}
