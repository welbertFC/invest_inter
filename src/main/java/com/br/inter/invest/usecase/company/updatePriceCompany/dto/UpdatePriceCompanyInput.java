package com.br.inter.invest.usecase.company.updatePriceCompany.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdatePriceCompanyInput implements Serializable {

    private UUID hash;
    private BigDecimal price;
}
