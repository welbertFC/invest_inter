package com.br.inter.invest.usecase.purchase.createPurchase.dto;

import com.br.inter.invest.controller.purchaseOrder.dto.CompanyPurchaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOutput {

    private String name;
    private String cpfcnpj;
    private Set<CompanyPurchaseDto> companies;
    private BigDecimal totalValue;
    private BigDecimal changeMoney;
}
