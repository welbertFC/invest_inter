package com.br.inter.invest.usecase.purchase.createPurchase;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.service.PurchaseOrderService;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.AcquiredCompaniesDto;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseOutput;
import com.br.inter.invest.usecase.purchase.createPurchase.mapper.CreatePurchaseMapper;
import com.br.inter.invest.usecase.purchase.createPurchase.validation.ValidateCreatePurchase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CreatePurchaseUseCase {

    private final CompanyService companyService;
    private final PurchaseOrderService service;
    private final List<ValidateCreatePurchase> validate;

    public PurchaseOutput execute(PurchaseInput input) {
        validate.forEach(it -> it.validate(input));
        AcquiredCompaniesDto listCompanies = getListCompanies(input);
        PurchaseOrderEntity purchaseOrder = CreatePurchaseMapper.convertInputToEntity(input, listCompanies);
        purchaseOrder.generatedHash();
        PurchaseOrderEntity purchaseOrderEntity = service.persistedPurchaseOrder(purchaseOrder);

        return CreatePurchaseMapper.convertEntityToOutput(purchaseOrderEntity);
    }

    private AcquiredCompaniesDto getListCompanies(PurchaseInput input) {
        List<CompanyEntity> companyPersisted = companyService.listCompanyOrderByPrice();
        return buyCompanies(input.getValue(), new ArrayList<>(), companyPersisted, 0, input.getQuantities());
    }

    private AcquiredCompaniesDto buyCompanies(BigDecimal value,
                                              List<CompanyEntity> list,
                                              List<CompanyEntity> companyPersisted,
                                              Integer index,
                                              Integer quantities) {
        if (index >= companyPersisted.size()) {
            return buyCompanies(value, list, companyPersisted, 0, quantities);
        } else if (quantities <= createSetCompanies(list).size()) {
            return newCompaniesPersistedByList(value, list, createSetCompanies(list));
        } else if (value.compareTo(companyPersisted.get(index).getPrice()) >= 0) {
            return addCompanyToList(value, list, companyPersisted, index, quantities);
        } else if (value.compareTo(companyPersisted.get(0).getPrice()) >= 1) {
            return buyCompanies(value, list, companyPersisted, 0, quantities);
        } else return factoryAcquiredCompaniesDto(value, list);
    }

    private Set<CompanyEntity> createSetCompanies(List<CompanyEntity> list) {
        return new HashSet<>(list);
    }

    private AcquiredCompaniesDto factoryAcquiredCompaniesDto(BigDecimal value, List<CompanyEntity> list) {
        return new AcquiredCompaniesDto(list, value);
    }

    private AcquiredCompaniesDto addCompanyToList(BigDecimal value, List<CompanyEntity> list, List<CompanyEntity> companyPersisted, Integer index, Integer quantities) {
        list.add(companyPersisted.get(index));
        BigDecimal realValue = value.subtract(companyPersisted.get(index).getPrice());
        return buyCompanies(realValue, list, companyPersisted, index + 1, quantities);
    }

    private AcquiredCompaniesDto newCompaniesPersistedByList(BigDecimal value,
                                                             List<CompanyEntity> list,
                                                             Set<CompanyEntity> entitySet) {
        return buyCompanies(value, list, new ArrayList<>(entitySet).stream()
                .sorted(Comparator.comparingDouble(company ->
                        company.getPrice().doubleValue())).collect(Collectors.toList()), 0, Integer.MAX_VALUE);
    }
}