package com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.mapper;

import com.br.inter.invest.controller.purchaseOrder.dto.CompanyPurchaseDto;
import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ListPurchaseMapper {

    private ListPurchaseMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static PurchaseResponse convertOutputToResponse(ListPurchaseOutput output) {
        return modelMapper.map(output, PurchaseResponse.class);
    }

    public static ListPurchaseOutput convertEntityToOutput(PurchaseOrderEntity entity) {
        return ListPurchaseOutput.builder()
                .name(entity.getName())
                .cpfcnpj(entity.getCpfcnpj())
                .companies(factoryPurchaseDto(entity.getCompanies()))
                .changeMoney(entity.getMoneyChange())
                .totalValue(entity.getCompanies().stream().map(CompanyEntity::getPrice)
                        .reduce(BigDecimal.ZERO, BigDecimal::add)).build();
    }

    public static Set<CompanyPurchaseDto> factoryPurchaseDto(List<CompanyEntity> entityList) {
        return entityList.stream().map(company -> new CompanyPurchaseDto(
                company.getName(),
                company.getTicker(),
                (int) entityList.stream().filter(it -> it.getHash().equals(company.getHash())).count(),
                entityList.stream().filter(it -> it.getHash().equals(company.getHash()))
                        .map(CompanyEntity::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add))

        ).collect(Collectors.toSet());
    }
}
