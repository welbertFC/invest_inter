package com.br.inter.invest.usecase.purchase.createPurchase.validation;

import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import com.br.inter.invest.util.ValidationUseCase;

public interface ValidateCreatePurchase extends ValidationUseCase<PurchaseInput> {
}
