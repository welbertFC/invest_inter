package com.br.inter.invest.usecase.company.listCompany.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListCompanyOutput implements Serializable {

    private UUID hash;

    private String name;

    private String ticker;

    private BigDecimal price;

    private Boolean active;

    private LocalDateTime creation;

    private LocalDateTime updateAt;
}
