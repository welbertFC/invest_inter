package com.br.inter.invest.usecase.purchase.createPurchase.validation;

import com.br.inter.invest.exception.EmptyListException;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ValidateEmptyList implements ValidateCreatePurchase {

    public static final String THERE_IS_NO_COMPANY_REGISTERED = "there is no company registered";
    private final CompanyService companyService;

    @Override
    public void validate(PurchaseInput useCase) {
        if (companyService.listCompanyOrderByPrice().isEmpty()){
            throw new EmptyListException(THERE_IS_NO_COMPANY_REGISTERED);
        }
    }
}
