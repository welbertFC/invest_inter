package com.br.inter.invest.usecase.company.deleteCompany;

import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.deleteCompany.dto.DeleteCompanyInput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteCompanyUseCase {

    private final CompanyService service;

    public void execute(DeleteCompanyInput input) {
        service.delete(input.getHash());
    }
}
