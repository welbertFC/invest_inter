package com.br.inter.invest.usecase.company.createCompany.validation;

import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import com.br.inter.invest.util.ValidationUseCase;

public interface ValidateCreateCompany extends ValidationUseCase<CreateCompanyInput> {
}
