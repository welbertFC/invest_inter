package com.br.inter.invest.usecase.company.createCompany.validation;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.exception.IllegalBusinessStateObjectException;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@Component
public class ValidateTickerIsEquals implements ValidateCreateCompany {

    private final CompanyService service;

    @Override
    public void validate(CreateCompanyInput useCase) {
        List<CompanyEntity> companies = service.listCompany();
        useCase.setTicker(useCase.getTicker().toUpperCase(Locale.ROOT).trim());
        companies.forEach(company -> {
            if (useCase.getTicker().equals(company.getTicker())) {
                throw IllegalBusinessStateObjectException.tickerIsEquals();
            }
        });

    }
}
