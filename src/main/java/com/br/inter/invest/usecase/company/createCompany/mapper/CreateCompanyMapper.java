package com.br.inter.invest.usecase.company.createCompany.mapper;

import com.br.inter.invest.controller.company.dto.CompanyRequest;
import com.br.inter.invest.controller.company.dto.CompanyResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CreateCompanyMapper {

    private CreateCompanyMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static CompanyEntity convertInputToEntity(CreateCompanyInput input) {
        return modelMapper.map(input, CompanyEntity.class);
    }

    public static CreateCompanyOutput convertEntityToOutput(CompanyEntity entity) {
        return modelMapper.map(entity, CreateCompanyOutput.class);
    }

    public static CreateCompanyInput convertRequestToInput(CompanyRequest request) {
        return modelMapper.map(request, CreateCompanyInput.class);
    }

    public static CompanyResponse convertOutputToResponse(CreateCompanyOutput output) {
        return modelMapper.map(output, CompanyResponse.class);
    }
}
