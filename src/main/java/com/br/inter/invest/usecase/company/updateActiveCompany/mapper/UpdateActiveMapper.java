package com.br.inter.invest.usecase.company.updateActiveCompany.mapper;

import com.br.inter.invest.controller.company.dto.CompanyResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyInput;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UpdateActiveMapper {

    private UpdateActiveMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static UpdateActiveCompanyOutput convertEntityToOutput(CompanyEntity entity) {
        return modelMapper.map(entity, UpdateActiveCompanyOutput.class);
    }

    public static UpdateActiveCompanyInput convertRequestToInput(UUID hash, Boolean active) {
        return UpdateActiveCompanyInput.builder()
                .active(active)
                .hash(hash)
                .build();
    }

    public static CompanyResponse convertOutputToResponse(UpdateActiveCompanyOutput output) {
        return modelMapper.map(output, CompanyResponse.class);
    }


}
