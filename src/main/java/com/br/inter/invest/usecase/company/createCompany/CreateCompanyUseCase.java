package com.br.inter.invest.usecase.company.createCompany;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyInput;
import com.br.inter.invest.usecase.company.createCompany.dto.CreateCompanyOutput;
import com.br.inter.invest.usecase.company.createCompany.mapper.CreateCompanyMapper;
import com.br.inter.invest.usecase.company.createCompany.validation.ValidateCreateCompany;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.List;
import java.util.Locale;

@Service
@RequiredArgsConstructor
public class CreateCompanyUseCase {

    private final CompanyService service;
    private final List<ValidateCreateCompany> validate;

    public CreateCompanyOutput execute(CreateCompanyInput input) {
        executeValidate(input);
        CompanyEntity entity = formatCompanyEntity(input);
        entity.generatedHash();
        return CreateCompanyMapper.convertEntityToOutput(service.persistedCompany(entity));
    }

    private CompanyEntity formatCompanyEntity(CreateCompanyInput input) {
        input.setTicker(input.getTicker().toUpperCase(Locale.ROOT).trim());
        input.setPrice(input.getPrice().setScale(2, RoundingMode.HALF_UP));
        input.setActive(input.getActive() == null || input.getActive());
        return CreateCompanyMapper.convertInputToEntity(input);
    }

    private void executeValidate(CreateCompanyInput input) {
        validate.forEach(it -> it.validate(input));
    }
}
