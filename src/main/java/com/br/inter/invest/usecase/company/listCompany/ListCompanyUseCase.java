package com.br.inter.invest.usecase.company.listCompany;

import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyInput;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyOutput;
import com.br.inter.invest.usecase.company.listCompany.mapper.ListCompanyMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ListCompanyUseCase {

    private final CompanyService service;

    public Page<ListCompanyOutput> execute(ListCompanyInput input) {
        return service.listCompanyPageable(input.getActive(), input.getPageable())
                .map(ListCompanyMapper::convertEntityToOutput);
    }
}
