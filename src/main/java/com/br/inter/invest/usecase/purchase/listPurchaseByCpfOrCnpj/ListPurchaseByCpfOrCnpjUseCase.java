package com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj;

import com.br.inter.invest.service.PurchaseOrderService;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseInput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto.ListPurchaseOutput;
import com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.mapper.ListPurchaseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ListPurchaseByCpfOrCnpjUseCase {

    private final PurchaseOrderService service;

    public Page<ListPurchaseOutput> execute(ListPurchaseInput input) {
        return service.listPurchaseOrderCpfOrCnpj(input.getCpfcnpj(), input.getPageable())
                .map(ListPurchaseMapper::convertEntityToOutput);
    }
}
