package com.br.inter.invest.usecase.company.updateActiveCompany.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateActiveCompanyInput {

    private UUID hash;
    private Boolean active;
}
