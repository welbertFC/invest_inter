package com.br.inter.invest.usecase.company.listCompany.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ListCompanyInput {

    private Boolean active;
    private Pageable pageable;
}
