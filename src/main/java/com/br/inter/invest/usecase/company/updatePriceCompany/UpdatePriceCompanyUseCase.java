package com.br.inter.invest.usecase.company.updatePriceCompany;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyInput;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyOutput;
import com.br.inter.invest.usecase.company.updatePriceCompany.mapper.UpdatePriceMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;

@Service
@RequiredArgsConstructor
public class UpdatePriceCompanyUseCase {

    private final CompanyService service;

    public UpdatePriceCompanyOutput execute(UpdatePriceCompanyInput input) {
        CompanyEntity entity = service.findByHash(input.getHash());
        entity.setPrice(input.getPrice().setScale(2, RoundingMode.HALF_UP));
        return UpdatePriceMapper.convertEntityToOutput(service.persistedCompany(entity));
    }

}
