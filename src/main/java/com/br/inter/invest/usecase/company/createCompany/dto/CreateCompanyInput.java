package com.br.inter.invest.usecase.company.createCompany.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateCompanyInput implements Serializable {

    private String name;

    private String ticker;

    private BigDecimal price;

    private Boolean active;
}
