package com.br.inter.invest.usecase.purchase.createPurchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseInput {

    private String name;
    private String cpfcnpj;
    private Integer quantities;
    private BigDecimal value;
}
