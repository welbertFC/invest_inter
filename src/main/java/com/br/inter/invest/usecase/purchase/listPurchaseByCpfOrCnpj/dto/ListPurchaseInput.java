package com.br.inter.invest.usecase.purchase.listPurchaseByCpfOrCnpj.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ListPurchaseInput {

    private String cpfcnpj;
    private Pageable pageable;
}
