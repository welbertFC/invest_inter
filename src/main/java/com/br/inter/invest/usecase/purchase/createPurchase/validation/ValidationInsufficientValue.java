package com.br.inter.invest.usecase.purchase.createPurchase.validation;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.exception.InsufficientValueException;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ValidationInsufficientValue implements ValidateCreatePurchase {

    public static final String THE_MINIMUM_AMOUNT_TO_BUY_A_SHARE_IS = "The minimum amount to buy a share is ";
    private final CompanyService companyService;

    @Override
    public void validate(PurchaseInput useCase) {
        List<CompanyEntity> companyEntityList = companyService.listCompanyOrderByPrice();
        companyEntityList.forEach(company -> {
            if (useCase.getValue().compareTo(company.getPrice()) < 0) {
                throw new InsufficientValueException(THE_MINIMUM_AMOUNT_TO_BUY_A_SHARE_IS + company.getPrice());
            }
        });


    }
}
