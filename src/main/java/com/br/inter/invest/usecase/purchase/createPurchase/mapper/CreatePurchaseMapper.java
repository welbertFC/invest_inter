package com.br.inter.invest.usecase.purchase.createPurchase.mapper;

import com.br.inter.invest.controller.purchaseOrder.dto.CompanyPurchaseDto;
import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseRequest;
import com.br.inter.invest.controller.purchaseOrder.dto.PurchaseResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.AcquiredCompaniesDto;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseInput;
import com.br.inter.invest.usecase.purchase.createPurchase.dto.PurchaseOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CreatePurchaseMapper {

    private CreatePurchaseMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static PurchaseResponse convertOutputToResponse(PurchaseOutput output) {
        return modelMapper.map(output, PurchaseResponse.class);
    }

    public static PurchaseInput convertRequestToInput(PurchaseRequest request) {
        return modelMapper.map(request, PurchaseInput.class);
    }

    public static PurchaseOrderEntity convertInputToEntity(PurchaseInput input, AcquiredCompaniesDto companiesDto) {
        return PurchaseOrderEntity.builder()
                .name(input.getName())
                .cpfcnpj(input.getCpfcnpj())
                .companies(companiesDto.getCompanies())
                .investedAmount(input.getValue())
                .moneyChange(companiesDto.getChange()).build();
    }

    public static PurchaseOutput convertEntityToOutput(PurchaseOrderEntity entity) {
        return PurchaseOutput.builder()
                .name(entity.getName())
                .cpfcnpj(entity.getCpfcnpj())
                .companies(factoryPurchaseDto(entity.getCompanies()))
                .changeMoney(entity.getMoneyChange())
                .totalValue(entity.getCompanies().stream().map(CompanyEntity::getPrice)
                        .reduce(BigDecimal.ZERO, BigDecimal::add)).build();
    }


    public static Set<CompanyPurchaseDto> factoryPurchaseDto(List<CompanyEntity> entityList) {
        return entityList.stream().map(company -> new CompanyPurchaseDto(
                company.getName(),
                company.getTicker(),
                (int) entityList.stream().filter(it -> it.getHash().equals(company.getHash())).count(),
                entityList.stream().filter(it -> it.getHash().equals(company.getHash()))
                        .map(CompanyEntity::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add))

        ).collect(Collectors.toSet());
    }


}
