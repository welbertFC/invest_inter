package com.br.inter.invest.usecase.company.updateActiveCompany;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.service.CompanyService;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyInput;
import com.br.inter.invest.usecase.company.updateActiveCompany.dto.UpdateActiveCompanyOutput;
import com.br.inter.invest.usecase.company.updateActiveCompany.mapper.UpdateActiveMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateActiveCompanyUseCase {

    private final CompanyService service;

    public UpdateActiveCompanyOutput execute(UpdateActiveCompanyInput input) {
        CompanyEntity companyEntity = service.findByHash(input.getHash());
        companyEntity.setActive(input.getActive());
        return UpdateActiveMapper.convertEntityToOutput(service.persistedCompany(companyEntity));

    }

}
