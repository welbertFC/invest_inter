package com.br.inter.invest.usecase.purchase.createPurchase.dto;

import com.br.inter.invest.entity.CompanyEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AcquiredCompaniesDto {

    private List<CompanyEntity> companies;
    private BigDecimal change;
}
