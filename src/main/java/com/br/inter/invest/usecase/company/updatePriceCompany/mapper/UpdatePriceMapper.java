package com.br.inter.invest.usecase.company.updatePriceCompany.mapper;

import com.br.inter.invest.controller.company.dto.CompanyResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyInput;
import com.br.inter.invest.usecase.company.updatePriceCompany.dto.UpdatePriceCompanyOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;

@Component
public class UpdatePriceMapper {

    private UpdatePriceMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static UpdatePriceCompanyOutput convertEntityToOutput(CompanyEntity entity) {
        return modelMapper.map(entity, UpdatePriceCompanyOutput.class);
    }

    public static UpdatePriceCompanyInput convertRequestToInput(UUID hash, BigDecimal price) {
        return UpdatePriceCompanyInput.builder()
                .price(price)
                .hash(hash)
                .build();
    }

    public static CompanyResponse convertOutputToResponse(UpdatePriceCompanyOutput output) {
        return modelMapper.map(output, CompanyResponse.class);
    }


}
