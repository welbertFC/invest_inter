package com.br.inter.invest.usecase.company.listCompany.mapper;

import com.br.inter.invest.controller.company.dto.CompanyResponse;
import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.usecase.company.listCompany.dto.ListCompanyOutput;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ListCompanyMapper {

    private ListCompanyMapper() {

    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static ListCompanyOutput convertEntityToOutput(CompanyEntity entity) {
        return modelMapper.map(entity, ListCompanyOutput.class);
    }

    public static CompanyResponse convertOutputToResponse(ListCompanyOutput output) {
        return modelMapper.map(output, CompanyResponse.class);
    }
}
