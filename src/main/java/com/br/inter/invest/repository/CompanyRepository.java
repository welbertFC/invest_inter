package com.br.inter.invest.repository;

import com.br.inter.invest.entity.CompanyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {

    Page<CompanyEntity> findAllByActive(Boolean active, Pageable pageable);

    Optional<CompanyEntity> findByHash(UUID hash);

    List<CompanyEntity> findAllByActiveTrueOrderByPriceAsc();
}
