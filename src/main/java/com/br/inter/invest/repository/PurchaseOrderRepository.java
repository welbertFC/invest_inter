package com.br.inter.invest.repository;

import com.br.inter.invest.entity.PurchaseOrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrderEntity, Long> {


    Page<PurchaseOrderEntity> findBycpfcnpj(String cpfcnpj, Pageable pageable);


}
