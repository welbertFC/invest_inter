package com.br.inter.invest.service;

import com.br.inter.invest.entity.PurchaseOrderEntity;
import com.br.inter.invest.repository.PurchaseOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class PurchaseOrderService {

    private final PurchaseOrderRepository repository;

    @Transactional
    public PurchaseOrderEntity persistedPurchaseOrder(PurchaseOrderEntity entity) {
        return repository.save(entity);
    }

    public Page<PurchaseOrderEntity> listPurchaseOrderCpfOrCnpj(String cpfcnpj, Pageable pageable) {
        return repository.findBycpfcnpj(cpfcnpj, pageable);
    }
}
