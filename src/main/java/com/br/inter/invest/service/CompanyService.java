package com.br.inter.invest.service;

import com.br.inter.invest.entity.CompanyEntity;
import com.br.inter.invest.exception.ObjectNotFoundException;
import com.br.inter.invest.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository repository;

    @Transactional
    @Caching(evict = {
            @CacheEvict(value = "listCompanyPageable", allEntries = true),
            @CacheEvict(value = "listCompany", allEntries = true),
            @CacheEvict(value = "listCompanyOrderByPrice", allEntries = true),
    })
    public CompanyEntity persistedCompany(CompanyEntity companyEntity) {
        return repository.save(companyEntity);
    }

    @Cacheable("listCompanyPageable")
    public Page<CompanyEntity> listCompanyPageable(Boolean active, Pageable pageable) {
        if (active != null) return repository.findAllByActive(active, pageable);
        else return repository.findAll(pageable);
    }

    @Cacheable("listCompany")
    public List<CompanyEntity> listCompany() {
        return repository.findAll();
    }

    public CompanyEntity findByHash(UUID hash) {
        return repository.findByHash(hash).orElseThrow(ObjectNotFoundException::new);
    }

    @Cacheable("listCompanyOrderByPrice")
    public List<CompanyEntity> listCompanyOrderByPrice() {
        return repository.findAllByActiveTrueOrderByPriceAsc();
    }

    @Transactional
    @Caching(evict = {
            @CacheEvict(value = "listCompanyPageable", allEntries = true),
            @CacheEvict(value = "listCompany", allEntries = true),
            @CacheEvict(value = "listCompanyOrderByPrice", allEntries = true),
    })
    public void delete(UUID hash) {
        CompanyEntity company = findByHash(hash);
        repository.delete(company);
    }
}
