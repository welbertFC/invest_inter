# invest_inter

Desafio Inter

## Para iniciar o projeto

- Navegue até seu workspace.
- Clone o repositório: `git clone git@gitlab.com:welbertFC/invest_inter.git`
- Para realizar o build execute o comando `./gradlew build`
- Para execuar a aplicação `./gradlew bootrun`
- Para executar testes `./gradlew test`

## Links Úteis

- Swagger: http://localhost:8080/swagger-ui/index.html#/
- Banco de Dados: http://localhost:8080/h2-console/